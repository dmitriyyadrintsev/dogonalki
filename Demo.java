package dogonalki;

public class Demo {
    public static void main(String[] args) {

        Dogonalki dogonalki = new Dogonalki(); //Создание потока
        Thread thread = Thread.currentThread();
        thread.setPriority(10);
        dogonalki.start(); //Запуск потока

        for (int i = 0; i < 100; i++) {

            try {
                Thread.sleep(1000); //Приостанавливает поток на 1 секунду
            } catch (InterruptedException e) {
            }

            System.out.println("Поток1 - " + i);
            if (i == 32) {
                thread.setPriority(1);
            }
        }

    }
}